require 'active_support/inflector'
require 'dotenv'
require 'fileutils'
require 'page-object'
require 'pathname'
require 'pry-byebug'
require 'rspec'
require 'selenium-webdriver'

Dir['./pages/**/*.rb'].each { |file| require file }

Before do
  Dotenv.load

  @driver = Selenium::WebDriver.for(:chrome)
  Watir.default_timeout = 10
  @driver.manage.timeouts.implicit_wait = 10
  @driver.manage.window.maximize

  Dir.glob(File.join('./pages/**/*.rb')).each do |file|
    page_object_class = Object.const_get(File.basename(file, '.rb').camelize)
    instance_variable_set("@#{File.basename(file, '.rb').downcase}", page_object_class.new(@driver))
  end
end

After do
  @driver.quit
end
