class GhostAdminLoginPage
  include PageObject

  text_field(:email_field, id: 'identification')
  text_field(:password_field, id: 'password')
  button(:sign_in_button, id: 'ember5')

  def enter_email(email)
    self.email_field = email
  end

  def enter_password(password)
    self.password_field = password
  end
end
