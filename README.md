# Qwick SDET Take Home

This project is a slimmed down example of Qwick's test automation repository. The project uses [Selenium](https://www.selenium.dev/) as the test framework and is written in Ruby using the [Page Object gem](https://github.com/cheezy/page-object) to help with setting up pages. Test scripts are written in [Cucumber](https://cucumber.io/). This project assumes that you have a local instance of the free CMS, [Ghost](https://github.com/TryGhost/Ghost), running on your machine.

## Getting Started:

### Prerequisites:
1. Install and run Ghost locally. Prerequisites and instructions to do this can be found [here](https://ghost.org/docs/install/local/)
1. Ruby v3.2.2 - Install Ruby on your machine and use a tool like [RVM](https://rvm.io/) to manage the version. You may also need to install bundler as well, `gem install bundler`
1. [Google Chrome](https://www.google.com/chrome/) (You can modify the browser used in `env.rb`)

### Running the project:
1. Clone a copy of this repository
1. In the project root directory run `bundle install`, to get all of the dependencies
1. Make a copy of the `.env` file: `cp .env_sample.env .env`
1. Navigate to the Ghost Admin site: [http://localhost:2368/ghost](http://localhost:2368/ghost). You should be prompted to make an account, make an account. In your newly created `.env` file, save your email and password in `GHOST_ADMIN_EMAIL` & `GHOST_ADMIN_PASSWORD` respectively
1. To run the project, run `cucumber`. This will execute the sample test provided in `ghost_admin_login.feature`

## Assignment:
Now it's time to roll up those sleeves and show off your testing skills! Using your clone of the project, please complete the following tasks. For questions that ask for an explanation, you can provide the response via email, a separate text document or inline in this README file. When finished, please email Hailee a zipped up copy of your repository.

* **Task 1:** Review the provided test to familiarize yourself with the structure of this project. Feel free to modify or update the existing test if you notice anything that can be improved.

* **Task 2:** We'd like you to add a test that does the following: Signs into the Ghost Admin site, Adds a new blog post, Validates the post is created and appears on the Ghost Blog website.

* **Task 3:** Add a few more tests of your choice to this project. Once done, please provide an explanation of the tests you added as well as why you added these tests.

* **Task 4:** Imagine you've been working closely with the team responsible for this testing framework as well as the CMS and Admin sites. Based on your experience working within this framework, we'd like to hear your thoughts. We're interested in any feedback you have regarding the tools, frameworks, practices, or any other aspects of the project. Please share your insights on what you found effective, any challenges you faced, and any improvements or innovations you might suggest. Feel free to discuss any aspect of the project that stood out to you, whether it be the workflow, tools, code structure, etc.\
\
There are no right or wrong answers to this prompt as we value all forms of feedback. Whether your suggestions address small tweaks or major overhauls, we are particularly interested in your unique perspective as someone who has actively engaged with this project.
