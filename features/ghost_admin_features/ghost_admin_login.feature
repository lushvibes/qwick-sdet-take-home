Feature: Ghost Admin Login

  Scenario: Sign into the Ghost Admin Site
    Given I navigate to the Ghost Admin home page
    And I enter my email and password
    When I click Sign in
    Then I expect to be in the Ghost Admin Dashboard
