Given('I navigate to the Ghost Admin home page') do
  @driver.navigate.to(ENV['GHOST_ADMIN_URL'])
end

Given('I enter my email and password') do
  @ghost_admin_login_page.enter_email(ENV['GHOST_ADMIN_EMAIL'])
  @ghost_admin_login_page.enter_password(ENV['GHOST_ADMIN_PASSWORD'])
end

When('I click Sign in') do
  @ghost_admin_login_page.sign_in_button
end

Then('I expect to be in the Ghost Admin Dashboard') do
  expect(1).to eq(1)
end
